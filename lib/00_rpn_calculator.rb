class RPNCalculator
  attr_accessor :stack, :value
  def initialize
    @stack = []
    @value = nil
  end

  def push(num)
    @stack << num
  end

  def plus
    raise("calculator is empty") if @stack.empty?
    @value = @stack.pop + @stack.pop
    @stack << @value
  end

  def minus
    raise("calculator is empty") if @stack.empty?
    first = @stack.pop
    second = @stack.pop
    @value = second - first
    @stack << @value
  end

  def divide
    raise("calculator is empty") if @stack.empty?
    first = @stack.pop
    second = @stack.pop
    @value = second.to_f / first.to_f
    @stack << @value
  end

  def times
    raise("calculator is empty") if @stack.empty?
    @value = @stack.pop.to_f * @stack.pop.to_f
    @stack << @value
  end

  def tokens(string)
    token_arr = []
    string.split.each do |item|
      if item.scan(/\D/).empty?
        token_arr << item.to_i
      else
        token_arr << item.to_sym
      end
    end
    token_arr
  end

  def evaluate(string)
    self.tokens(string).each do |token|
      case token
        when :+
          self.plus
        when :-
          self.minus
        when :/
          self.divide
        when :*
          self.times
        else
          self.push(token)
      end
    end
    self.value
  end
end
